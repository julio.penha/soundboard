#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "./ui_mainwindow.h"
#include <QMainWindow>
#include <QSound>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

public slots:
  void playSound1();
  void playSound2();
  void playSound3();
  void playSound4();
  void playSound5();
  void playSound6();
  void playSound7();
  void playSound8();
  void playSound9();

private:
  Ui::MainWindow *ui;
};
#endif // MAINWINDOW_HPP
