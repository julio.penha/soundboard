#include "mainwindow.hpp"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  connect(ui->btn1, &QPushButton::clicked, this, &MainWindow::playSound1);
  connect(ui->btn2, &QPushButton::clicked, this, &MainWindow::playSound2);
  connect(ui->btn3, &QPushButton::clicked, this, &MainWindow::playSound3);
  connect(ui->btn4, &QPushButton::clicked, this, &MainWindow::playSound4);
  connect(ui->btn5, &QPushButton::clicked, this, &MainWindow::playSound5);
  connect(ui->btn6, &QPushButton::clicked, this, &MainWindow::playSound6);
  connect(ui->btn7, &QPushButton::clicked, this, &MainWindow::playSound7);
  connect(ui->btn8, &QPushButton::clicked, this, &MainWindow::playSound8);
  connect(ui->btn9, &QPushButton::clicked, this, &MainWindow::playSound9);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::playSound1() { QSound::play(":/sounds/audio1.wav"); }
void MainWindow::playSound2() { QSound::play(":/sounds/audio2.wav"); }
void MainWindow::playSound3() { QSound::play(":/sounds/audio3.wav"); }
void MainWindow::playSound4() { QSound::play(":/sounds/audio4.wav"); }
void MainWindow::playSound5() { QSound::play(":/sounds/audio5.wav"); }
void MainWindow::playSound6() { QSound::play(":/sounds/audio6.wav"); }
void MainWindow::playSound7() { QSound::play(":/sounds/audio7.wav"); }
void MainWindow::playSound8() { QSound::play(":/sounds/audio8.wav"); }
void MainWindow::playSound9() { QSound::play(":/sounds/audio9.wav"); }
